# [![k8SrvTool header](https://viflcraft.tk/assets/img/header.png)](#)
# k8SrvTool

k8SrvTool is a tool that allows you to run your own Minecraft server using openjdk 11 and aikars flags, even if your hosting doesn't support these!


## How does k8SrvTool work?
k8SrvTool will download and use a portable version of openjdk 11 (unless you are already running openjdk 11 or newer) and the latest stable build of Yatopia (but soon you will be able to choose other server software such as paper) and use Aikars flags to execute your Minecraft Server, this provides maximum performance and makes sure you are always secure with the latest updates!

k8SrvTool is compiled and built using openjdk 8 therefore there should be no problems running it!

## Installation & usage

**Note: if you're trying to run this on hosting that uses pterodactyl or any other panel you probably don't need to do the steps described below but instead you should rename the k8SrvToolsG jar file to server.jar if you don't have the option to change the startup file name in startup parameters**

if you don't want to build your own version from source you can just download one of our prebuilt releases by clicking [here](https://gitlab.com/a8_/k8srvtool/-/releases)

all you need to do now is to execute the jar file that you just downloaded and the rest will be taken care of
```bash
java -jar k8SrvTools-1.0.jar
```
## Building
On Unix-like systems building should be straightforward all you need to do is just to run the gradlew build command in projects root directory.
```bash
./gradlew build
```
## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[AGPLv3](https://choosealicense.com/licenses/agpl-3.0/)
