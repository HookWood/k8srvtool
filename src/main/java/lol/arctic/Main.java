package lol.arctic;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.commons.io.FileUtils;
import org.rauschig.jarchivelib.Archiver;
import org.rauschig.jarchivelib.ArchiverFactory;

import java.io.*;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Objects;
import java.util.concurrent.TimeUnit;


import static java.lang.System.exit;

public class Main {

    //TODO: handle chat colors and input and make config


    public static void main(String[] args) throws IOException {
        //declare it up here for scope resions
        String pathToJava;

        System.out.println("k8SrvTool v1.0 By Kamey and Arctic5824");


        while (true) {
            //why is all this code in loop? because if server crashes it wil auto restart :D

            long rambytes = Runtime.getRuntime().maxMemory() - 1000000000;
            int ram = (int) (rambytes / 1024);

            System.out.println("running with " + ram + "kb" + " this should be 1GB less then your host gives you");


            //check if server.jar exists and delete it if it does, we will redownload it to make sure its latest ver
            File serverJar = new File(System.getProperty("user.dir") + "/server1.jar");
            if (serverJar.exists()) {
                //we make sure delete goes well
                if (!(serverJar.delete())) {
                    System.out.println("there was an error deleting the server.jar file. please retry, if this issue persists try deleting the file manually, if this issue still persists contact, Arctic#5824 or Kamey#3257 on discord or send us an email at nikitadd22@gmail.com or tesla.vm123@gmail.com");
                }
            }
            //lets download latest yatopia TODO: make config option to choose server software AND VERSION, CURRENTLY THIS IS LOCKED AT 1.16.5



            final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();

            Request request = new Request.Builder()
                    .url("https://api.yatopiamc.org/v2/stableBuild/download?branch=ver/1.16.5")
                    .build();

            try (Response response = okHttpClient.newCall(request).execute()) {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                byte[] bytes =   Objects.requireNonNull(response.body()).bytes();
                OutputStream writeJar = new FileOutputStream(serverJar);
                writeJar.write(bytes);
                writeJar.close();
            }

            //check if java11 is being used to run our program then do proper thing to do

            String javaVer = System.getProperty("java.specification.version");
            if (!(javaVer.equals("1.8") || javaVer.equals("9") || javaVer.equals("10"))) {
                System.out.println("Already running on java 11 or newer :), skipping downloading and usage of portable version of java 11");
                System.out.println();
                pathToJava = "/bin/";
            } else {
                System.out.println("not running on java 11, portable version of java 11 will be automatically downloaded and used, please wait a few minutes");

                //make sure the folder for java11 does not exist yet!
                File javaInstallFolder = new File(System.getProperty("user.dir") + "/javaInstall");
                if (javaInstallFolder.exists()) {
                    //useing apache fileutils to delete the dir, might not be worth adding apache file utils to project just for this but i can rewrite doing this if needed
                    FileUtils.deleteDirectory(javaInstallFolder);
                }

                //   alright lets get java11

                //   make the folder to extract java to

                boolean bool = javaInstallFolder.mkdir();
                //if it failed to make folder, bool returns true if there is no issues, so if bool is not true there is issue
                if (!bool) {
                    System.out.println("Sorry couldn't create specified directory");
                    exit(2);
                }

                //okay we have folder created now
                //lets download java 11
                InputStream inputStream = new URL("https://api.adoptopenjdk.net/v3/binary/latest/11/ga/linux/x64/jdk/hotspot/normal/adoptopenjdk?project=jdk").openStream();
                Files.copy(inputStream, Paths.get(System.getProperty("user.dir") + "/javaInstall/java11.tar.gz"), StandardCopyOption.REPLACE_EXISTING);

                //cool now we have java 11 in System.getProperty("user.dir") + "/javaInstall/java11.tar.gz"

                Archiver archiver = ArchiverFactory.createArchiver("tar", "gz");
                //first input is archive, second is destantion
                archiver.extract(new File(System.getProperty("user.dir") + "/javaInstall/java11.tar.gz"), new File(System.getProperty("user.dir") + "/javaInstall/java11"));

                //okay extraction finished now delete tar.gz file
                File targzfile = new File(System.getProperty("user.dir") + "/javaInstall/java11.tar.gz");
                if (!(targzfile.delete())) {
                    System.out.println("Sorry there was an error deleting java11.tar.gz file);
                }

                //set java path to proper location


                //first we must get the folder for jdk name as it can change with updates to java

                File javaExtractDir = new File(System.getProperty("user.dir") + "/javaInstall/java11");

                String[] javaFolderArray = javaExtractDir.list();
                assert javaFolderArray != null;
                String javaFolder = javaFolderArray[0];

                pathToJava = System.getProperty("user.dir") + "/javaInstall/java11/" + javaFolder + "/bin/";


            }

            {
                //start server
                ProcessBuilder processBuilder = new ProcessBuilder();

                //make sure we can run java file
                processBuilder.command("chmod", "+x " + pathToJava + "java");

                //each diff parmator needs to be sperated via comma
                processBuilder.command(pathToJava + "java", "-Xms" + ram + "K", "-Xmx" + ram + "K", "-XX:+UseG1GC", "-XX:+ParallelRefProcEnabled", "-XX:MaxGCPauseMillis=200", "-XX:+UnlockExperimentalVMOptions", "-XX:+DisableExplicitGC", "-XX:+AlwaysPreTouch", "-XX:G1NewSizePercent=30", "-XX:G1MaxNewSizePercent=40", "-XX:G1HeapRegionSize=8M", "-XX:G1ReservePercent=20", "-XX:G1HeapWastePercent=5", "-XX:G1MixedGCCountTarget=4", "-XX:InitiatingHeapOccupancyPercent=15", "-XX:G1MixedGCLiveThresholdPercent=90", "-XX:G1RSetUpdatingPauseTimePercent=5", "-XX:SurvivorRatio=32", "-XX:+PerfDisableSharedMem", "-XX:MaxTenuringThreshold=1", "-Dusing.aikars.flags=https://mcflags.emc.gs", "-Daikars.new.flags=true", "-jar", String.valueOf(serverJar), "nogui");

                try {


                    processBuilder.redirectOutput(ProcessBuilder.Redirect.INHERIT);
                    processBuilder.redirectInput(ProcessBuilder.Redirect.INHERIT);
                    Process process = processBuilder.start();


                    int exitCode = process.waitFor();

                    if (exitCode != 0) {
                        System.out.println("server with exit code " + exitCode);
                        break;
                    } else {
                        System.out.println("Server closed");
                    }


                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }

            }


        }
    }


}

